import React from "react";
import "../App.css";
import Blood1 from "../images/Blood1.png";
import bank from "../images/bank.png";
function FirstPage() {
  return (
    <div className="first-page">
      <img className="img1" src={Blood1} alt="" srcset="" />
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
        }}
      >
        <div className="box1">
          <div style={{ display: "flex" }}>
            <img className="sm-img" src={bank} alt="" />
            <div className="sm-font">3023</div>
          </div>
          <div className="lg-font">Blood Banks</div>
        </div>
        <div className="box1">
          <div style={{ display: "flex" }}>
            <img className="sm-img" src={bank} alt="" />
            <div className="sm-font">75.44</div>
          </div>
          <div className="lg-font">Million Units collected</div>
        </div>
        <div className="box1">
          <div style={{ display: "flex" }}>
            <img className="sm-img" src={bank} alt="" />
            <div className="sm-font">76%</div>
          </div>
          <div className="lg-font">
            Blood Collected from <br /> Voluntary Blood Donors
          </div>
        </div>
      </div>
    </div>
  );
}

export default FirstPage;
