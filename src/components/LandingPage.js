import React from "react";
import FirstPage from "./FirstPage";
import Header from "./Header";
import SecondPage from "./SecondPage";
import ThirdPage from "./ThirdPage";

function LandingPage() {
  return (
    <div>
      <Header></Header>
      <FirstPage></FirstPage>
      <SecondPage></SecondPage>
      <ThirdPage></ThirdPage>
    </div>
  );
}

export default LandingPage;
