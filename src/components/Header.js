import React from "react";
import "../App.css";
import blud from "../images/blud.png";
import appstore from "../images/appstore.png";
import googlestore from "../images/googlestore.png";

function Header() {
  return (
    <div className="header">
      <div className="child-header">
        <div style={{ display: "flex" }}>
          <img
            style={{ width: 92, height: 30, marginRight: 10 }}
            src={blud}
            alt=""
          />
          <div className="tabs ">About</div>
          <div className="tabs">Products</div>
          <div className="tabs">More</div>
        </div>
        <div style={{ display: "flex" }}>
          <img
            className="logos"
            href="www.google.com"
            src={googlestore}
            alt=""
          />
          <img className="logos" src={appstore} alt="" />
        </div>
      </div>
    </div>
  );
}

export default Header;
